FROM python:latest
WORKDIR /lab2
COPY server.py ./
COPY output/ ./output/
RUN pip install mysql-connector-python
CMD ["python3", "./server.py"]
