CREATE DATABASE lab_2_db;
 
USE lab_2_db;
 
CREATE TABLE students (
  student_id mediumint(8) NOT NULL auto_increment,
  name varchar(255) default NULL,
  date_of_birth varchar(255),
  PRIMARY KEY (student_id)
) ;
 
CREATE TABLE gradebooks(
  gradebook_id varchar(255),
  student_id mediumint,
  subject TEXT default NULL,
  date_of_exam varchar(255) default NULL,
  name varchar(255) default NULL,
  mark mediumint default NULL
);
 

INSERT INTO students VALUES ( 1 ,'  Алтухова Елизавета Геннадьевна  ',' 2003-09-02  '),
( 2 ,'  Болгарь Александра Андреевна  ',' 2006-08-07  '),
( 3 ,'  Буриков Сергей Сергеевич  ',' 2005-07-07  '),
( 4 ,'  Бушина Юлиана Вячеславовна  ',' 2006-05-04  '),
( 5 ,'  Ваганов Владимир Геннадьевич  ',' 2005-12-22  '),
( 6 ,'  Винников Алексей Валерьевич ',' 2003-06-21  '),
( 7 ,'  Горнев Кирилл Владимирович  ',' 2006-07-22  '),
( 8 ,'  Грачев Егор Владимирович  ',' 2003-03-21  '),
( 9 ,'  Гусаров Иван Владимирович ',' 2006-11-28  '),
( 10  ,'  Джумъаев Абдувохид  ',' 2006-05-14  '),
( 11  ,'  Евтеев Даниил Олегович  ',' 2006-08-23  '),
( 12  ,'  Жилов Никита Витальевич ',' 2005-07-26  '),
( 13  ,'  Зимин Михаил Максимович ',' 2006-02-23  '),
( 14  ,'  Злотов Андрей Викторович  ',' 2005-04-08  '),
( 15  ,'  Игнатенко Денис Анатольевич ',' 2005-07-16  '),
( 16  ,'  Качалов Максим Сергеевич  ',' 2005-03-18  '),
( 17  ,'  Козяков Ярослав Евгеньевич  ',' 2003-02-08  '),
( 18  ,'  Корнилов Андрей Олегович  ',' 2006-11-29  '),
( 19  ,'  Костюшов Виталий Владимирович ',' 2005-01-01  '),
( 20  ,'  Кудряшов Александр Владимирович ',' 2006-07-18  '),
( 21  ,'  Линник Игорь Алексеевич ',' 2004-12-20  '),
( 22  ,'  Локтев Владимир Дмитриевич  ',' 2004-04-25  '),
( 23  ,'  Матяш Никита Дмитриевич ',' 2004-03-01  '),
( 24  ,'  Мещанкин Дмитрий Ильич  ',' 2004-01-08  '),
( 25  ,'  Монахова Виктория Ивановна  ',' 2003-08-04  '),
( 26  ,'  Муратов Кирилл Витальевич ',' 2004-08-30  '),
( 27  ,'  Наваб Шахмир  ',' 2003-08-04  '),
( 28  ,'  Никифоров Иван Максимович ',' 2003-11-25  '),
( 29  ,'  Пискунов Михаил Сергеевич ',' 2003-09-21  '),
( 30  ,'  Рошади Акира Ризки  ',' 2004-07-14  '),
( 31  ,'  Сморчков Михаил Максимович  ',' 2005-02-23  '),
( 32  ,'  Стеблин Егор Владиславович  ',' 2005-06-03  '),
( 33  ,'  Узлов Даниил Юрьевич  ',' 2003-10-29  '),
( 34  ,'  Фоминых Александр Евгеньевич  ',' 2006-09-14  '),
( 35  ,'  Цыгановкин Илья Олегович  ',' 2004-07-13  '),
( 36  ,'  Шандыкова Виктория Константиновна ',' 2004-02-08  ');

INSERT INTO gradebooks VALUES("19Б0179",1,"Моделирование систем","2023-01-13","Набадчиков Петр Иннокентиевич",3)  ,
("19Б0515",7,"Моделирование систем","2023-01-31","Набадчиков Петр Иннокентиевич",5) ,
("19Б0571",9,"Моделирование систем","2023-01-26","Набадчиков Петр Иннокентиевич",4) ,
("19Б0627",10,"Моделирование систем","2022-12-26","Набадчиков Петр Иннокентиевич",3)  ,
("19Б0683",11,"Моделирование систем","2023-01-18","Набадчиков Петр Иннокентиевич",5)  ,
("19Б0739",12,"Моделирование систем","2023-01-27","Набадчиков Петр Иннокентиевич",2)  ,
("19Б0795",13,"Моделирование систем","2023-01-20","Набадчиков Петр Иннокентиевич",3)  ,
("19Б0851",14,"Моделирование систем","2023-01-17","Набадчиков Петр Иннокентиевич",NULL) ,
("19Б0907",15,"Моделирование систем","2023-01-18","Набадчиков Петр Иннокентиевич",4)  ;
 
INSERT INTO gradebooks VALUES
("19Б0963",16,"Моделирование систем","2023-01-08","Набадчиков Петр Иннокентиевич",4)  ,
("19Б01019",17,"Моделирование систем","2023-01-29","Набадчиков Петр Иннокентиевич",3) ,
("19Б01075",18,"Моделирование систем","2023-01-23","Набадчиков Петр Иннокентиевич",2) ,
("19Б01131",19,"Моделирование систем","2022-12-31","Набадчиков Петр Иннокентиевич",4) ,
("19Б01187",20,"Моделирование систем","2023-01-26","Набадчиков Петр Иннокентиевич",NULL)  ,
("19Б01243",21,"Моделирование систем","2023-01-11","Набадчиков Петр Иннокентиевич",3) ,
("19Б01299",22,"Моделирование систем","2023-01-25","Набадчиков Петр Иннокентиевич",2) ,
("19Б01355",23,"Моделирование систем","2023-01-14","Набадчиков Петр Иннокентиевич",3) ,
("19Б01411",24,"Моделирование систем","2023-01-08","Набадчиков Петр Иннокентиевич",4) ,
("19Б01467",25,"Моделирование систем","2023-01-19","Набадчиков Петр Иннокентиевич",NULL)  ,
("19Б01523",26,"Моделирование систем","2023-01-13","Набадчиков Петр Иннокентиевич",5) ,
("19Б01579",27,"Моделирование систем","2022-12-25","Набадчиков Петр Иннокентиевич",4) ,
("19Б01635",28,"Моделирование систем","2022-12-30","Набадчиков Петр Иннокентиевич",3) ,
("19Б01691",29,"Моделирование систем","2023-01-20","Набадчиков Петр Иннокентиевич",4) ,
("19Б01747",30,"Моделирование систем","2022-12-28","Набадчиков Петр Иннокентиевич",3) ,
("19Б01803",31,"Моделирование систем","2023-01-13","Набадчиков Петр Иннокентиевич",3) ,
("19Б01859",32,"Моделирование систем","2023-01-19","Набадчиков Петр Иннокентиевич",NULL)  ;
 
INSERT INTO gradebooks VALUES
("19Б02027",35,"Моделирование систем","2023-01-31","Набадчиков Петр Иннокентиевич",4) ,
("19Б02083",36,"Моделирование систем","2023-01-12","Набадчиков Петр Иннокентиевич",2) ,
("19Б0123",8,"Программные средства манипулирования данными","2022-12-27","Котилевец Игорь Денисович",3) ,
("19Б0179",1,"Программные средства манипулирования данными","2023-01-14","Котилевец Игорь Денисович",4) ,
("19Б0235",2,"Программные средства манипулирования данными","2023-01-03","Котилевец Игорь Денисович",4) ,
("19Б0291",3,"Программные средства манипулирования данными","2023-01-22","Котилевец Игорь Денисович",4) ,
("19Б0459",6,"Программные средства манипулирования данными","2023-01-13","Котилевец Игорь Денисович",2) ,
("19Б0515",7,"Программные средства манипулирования данными","2022-12-31","Котилевец Игорь Денисович",NULL)  ,
("19Б0739",12,"Программные средства манипулирования данными","2023-01-30","Котилевец Игорь Денисович",5)  ,
("19Б0795",13,"Программные средства манипулирования данными","2023-01-12","Котилевец Игорь Денисович",4)  ,
("19Б0851",14,"Программные средства манипулирования данными","2023-01-01","Котилевец Игорь Денисович",3)  ,
("19Б0907",15,"Программные средства манипулирования данными","2023-01-26","Котилевец Игорь Денисович",2)  ,
("19Б0963",16,"Программные средства манипулирования данными","2022-12-29","Котилевец Игорь Денисович",5)  ,
("19Б01131",19,"Программные средства манипулирования данными","2023-01-24","Котилевец Игорь Денисович",5) ,
("19Б01187",20,"Программные средства манипулирования данными","2023-01-12","Котилевец Игорь Денисович",NULL)  ,
("19Б01243",21,"Программные средства манипулирования данными","2023-01-23","Котилевец Игорь Денисович",3) ,
("19Б01299",22,"Программные средства манипулирования данными","2023-01-18","Котилевец Игорь Денисович",3) ,
("19Б01355",23,"Программные средства манипулирования данными","2023-01-08","Котилевец Игорь Денисович",4) ,
("19Б01411",24,"Программные средства манипулирования данными","2023-01-03","Котилевец Игорь Денисович",3) ,
("19Б01467",25,"Программные средства манипулирования данными","2022-12-27","Котилевец Игорь Денисович",4) ,
("19Б01523",26,"Программные средства манипулирования данными","2023-01-29","Котилевец Игорь Денисович",3) ,
("19Б01579",27,"Программные средства манипулирования данными","2023-01-14","Котилевец Игорь Денисович",4) ,
("19Б01635",28,"Программные средства манипулирования данными","2022-12-31","Котилевец Игорь Денисович",NULL)  ,
("19Б01803",31,"Программные средства манипулирования данными","2023-01-31","Котилевец Игорь Денисович",NULL)  ,
("19Б01859",32,"Программные средства манипулирования данными","2023-01-29","Котилевец Игорь Денисович",3) ,
("19Б01915",33,"Программные средства манипулирования данными","2023-01-20","Котилевец Игорь Денисович",3) ,
("19Б01971",34,"Программные средства манипулирования данными","2023-01-21","Котилевец Игорь Денисович",5) ,
("19Б02027",35,"Программные средства манипулирования данными","2023-01-13","Котилевец Игорь Денисович",5) ;
INSERT INTO gradebooks VALUES
("19Б02083",36,"Программные средства манипулирования данными","2023-01-12","Котилевец Игорь Денисович",5) ,
("19Б0123",8,"Методы искусственного интеллекта","2023-01-03","Вырыпаев Кирилл Тимофеевич",5)  ,
("19Б0179",1,"Методы искусственного интеллекта","2023-01-10","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б0235",2,"Методы искусственного интеллекта","2023-01-09","Вырыпаев Кирилл Тимофеевич",2)  ,
("19Б0291",3,"Методы искусственного интеллекта","2023-01-29","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б0459",6,"Методы искусственного интеллекта","2023-01-16","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б0515",7,"Методы искусственного интеллекта","2023-01-26","Вырыпаев Кирилл Тимофеевич",3)  ,
("19Б0571",9,"Методы искусственного интеллекта","2023-01-05","Вырыпаев Кирилл Тимофеевич",NULL) ,
("19Б0627",10,"Методы искусственного интеллекта","2023-01-27","Вырыпаев Кирилл Тимофеевич",3) ,
("19Б0683",11,"Методы искусственного интеллекта","2023-01-07","Вырыпаев Кирилл Тимофеевич",4) ,
("19Б0907",15,"Методы искусственного интеллекта","2023-01-14","Вырыпаев Кирилл Тимофеевич",NULL)  ,
("19Б0963",16,"Методы искусственного интеллекта","2022-12-28","Вырыпаев Кирилл Тимофеевич",3) ,
("19Б01131",19,"Методы искусственного интеллекта","2023-01-27","Вырыпаев Кирилл Тимофеевич",2)  ,
("19Б01187",20,"Методы искусственного интеллекта","2022-12-26","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б01243",21,"Методы искусственного интеллекта","2023-01-17","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б01299",22,"Методы искусственного интеллекта","2023-01-04","Вырыпаев Кирилл Тимофеевич",NULL) ,
("19Б01355",23,"Методы искусственного интеллекта","2023-01-30","Вырыпаев Кирилл Тимофеевич",2)  ,
("19Б01411",24,"Методы искусственного интеллекта","2023-01-21","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б01467",25,"Методы искусственного интеллекта","2023-01-24","Вырыпаев Кирилл Тимофеевич",3)  ,
("19Б01523",26,"Методы искусственного интеллекта","2023-01-10","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б01579",27,"Методы искусственного интеллекта","2022-12-28","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б01635",28,"Методы искусственного интеллекта","2023-01-11","Вырыпаев Кирилл Тимофеевич",3)  ,
("19Б01803",31,"Методы искусственного интеллекта","2023-01-25","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б01859",32,"Методы искусственного интеллекта","2023-01-04","Вырыпаев Кирилл Тимофеевич",4)  ,
("19Б01915",33,"Методы искусственного интеллекта","2023-01-23","Вырыпаев Кирилл Тимофеевич",3)  ,
("19Б01971",34,"Методы искусственного интеллекта","2023-01-30","Вырыпаев Кирилл Тимофеевич",5)  ,
("19Б02027",35,"Методы искусственного интеллекта","2022-12-26","Вырыпаев Кирилл Тимофеевич",3)  ,
("19Б02083",36,"Методы искусственного интеллекта","2023-01-07","Вырыпаев Кирилл Тимофеевич",3)  ;
 
 
INSERT INTO gradebooks VALUES
("19Б0123",8,"Средства моделирования разработки программного обеспечения","2023-01-31","Лебедев Артем Сергеевич",4) ,
("19Б0179",1,"Средства моделирования разработки программного обеспечения","2022-12-30","Лебедев Артем Сергеевич",5) ,
("19Б0235",2,"Средства моделирования разработки программного обеспечения","2023-01-04","Лебедев Артем Сергеевич",NULL)  ,
("19Б0291",3,"Средства моделирования разработки программного обеспечения","2023-01-10","Лебедев Артем Сергеевич",3) ,
("19Б0459",6,"Средства моделирования разработки программного обеспечения","2023-01-16","Лебедев Артем Сергеевич",4) ,
("19Б0515",7,"Средства моделирования разработки программного обеспечения","2023-01-14","Лебедев Артем Сергеевич",5) ,
("19Б0571",9,"Средства моделирования разработки программного обеспечения","2023-01-01","Лебедев Артем Сергеевич",4) ,
("19Б0627",10,"Средства моделирования разработки программного обеспечения","2022-12-31","Лебедев Артем Сергеевич",NULL) ,
("19Б0683",11,"Средства моделирования разработки программного обеспечения","2023-01-08","Лебедев Артем Сергеевич",3)  ,
("19Б0739",12,"Средства моделирования разработки программного обеспечения","2023-01-15","Лебедев Артем Сергеевич",4)  ,
("19Б0795",13,"Средства моделирования разработки программного обеспечения","2022-12-26","Лебедев Артем Сергеевич",4)  ,
("19Б0851",14,"Средства моделирования разработки программного обеспечения","2023-01-20","Лебедев Артем Сергеевич",4)  ,
("19Б0907",15,"Средства моделирования разработки программного обеспечения","2023-01-16","Лебедев Артем Сергеевич",4)  ,
("19Б0963",16,"Средства моделирования разработки программного обеспечения","2023-01-14","Лебедев Артем Сергеевич",4)  ,
("19Б01019",17,"Средства моделирования разработки программного обеспечения","2023-01-01","Лебедев Артем Сергеевич",3) ,
("19Б01075",18,"Средства моделирования разработки программного обеспечения","2022-12-31","Лебедев Артем Сергеевич",5) ,
("19Б01131",19,"Средства моделирования разработки программного обеспечения","2023-01-08","Лебедев Артем Сергеевич",3) ,
("19Б01187",20,"Средства моделирования разработки программного обеспечения","2023-01-15","Лебедев Артем Сергеевич",3) ,
("19Б01243",21,"Средства моделирования разработки программного обеспечения","2022-12-26","Лебедев Артем Сергеевич",NULL)  ,
("19Б01299",22,"Средства моделирования разработки программного обеспечения","2023-01-20","Лебедев Артем Сергеевич",5) ,
("19Б01355",23,"Средства моделирования разработки программного обеспечения","2023-01-16","Лебедев Артем Сергеевич",4) ,
("19Б01411",24,"Средства моделирования разработки программного обеспечения","2023-01-14","Лебедев Артем Сергеевич",3) ,
("19Б01467",25,"Средства моделирования разработки программного обеспечения","2023-01-01","Лебедев Артем Сергеевич",NULL)  ,
("19Б01523",26,"Средства моделирования разработки программного обеспечения","2022-12-31","Лебедев Артем Сергеевич",NULL)  ,
("19Б01579",27,"Средства моделирования разработки программного обеспечения","2023-01-08","Лебедев Артем Сергеевич",4) ,
("19Б01803",31,"Средства моделирования разработки программного обеспечения","2023-01-16","Лебедев Артем Сергеевич",3) ,
("19Б01859",32,"Средства моделирования разработки программного обеспечения","2023-01-14","Лебедев Артем Сергеевич",4) ,
("19Б01915",33,"Средства моделирования разработки программного обеспечения","2023-01-01","Лебедев Артем Сергеевич",4) ,
("19Б01971",34,"Средства моделирования разработки программного обеспечения","2022-12-31","Лебедев Артем Сергеевич",4) ,
("19Б02027",35,"Средства моделирования разработки программного обеспечения","2023-01-08","Лебедев Артем Сергеевич",NULL)  ,
("19Б02083",36,"Средства моделирования разработки программного обеспечения","2023-01-15","Лебедев Артем Сергеевич",NULL)  ;
 
COMMIT;
