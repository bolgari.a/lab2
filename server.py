import mysql.connector

mydb = mysql.connector.connect(
  host="mysql-service",
  user="root",
  password="root",
  database="lab_2_db"
)

mycursor = mydb.cursor()

mycursor.execute("""
SELECT s.name, s.date_of_birth FROM students s
JOIN gradebooks g
ON s.student_id = g.student_id
WHERE g.subject = "Программные средства манипулирования данными"
ORDER BY g.mark DESC;
""")

result = mycursor.fetchall()
print(result)

f = open('output/result2.csv', 'w')

header = [row[0] for row in mycursor.description]

f.write(','.join(header) + '\n')
for row in result:
    f.write(','.join(str(r) for r in row) + '\n')
f.close()
mydb.close()
